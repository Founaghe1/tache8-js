// 1 - Créez un tableau de nombres

let tab = [12 , 23 , 36 , 43 , 55];
// alert(tab);
console.log(tab)

for (let i = 0; i < tab.length; i++) {
    console.log(`L'element à l'index ${i} est : ${tab[i]}`)
}

//1_utilisez la méthode .map() pour doubler chaque nombre du tableau.

console.log('1_utilisez la méthode .map() pour doubler chaque nombre du tableau.')

tab = tab.map(nombre => nombre * 2);
console.log(tab);

//2_Créez un tableau de chaînes et utilisez la méthode .filter() pour renvoyer un nouveau tableau avec uniquement les chaînes dont la longueur est supérieure à 3.

console.log('2_Créez un tableau de chaînes et utilisez la méthode .filter() pour renvoyer un nouveau tableau avec uniquement les chaînes dont la longueur est supérieure à 3.');

let chaine = ['lui', 'amad', 'moi', 'aminata', 'billy'];
console.log(chaine);

console.log('Nouveau tableau')
let filterChaine = chaine.filter(chaines => chaines.length > 3);
console.log(filterChaine);

//3_Utilisez la méthode .reduce() pour résumer tous les nombres dans un tableau de nombres. 

console.log('3_Utilisez la méthode .reduce() pour résumer tous les nombres dans un tableau de nombres. ')

let nombres = [3 , 4 , 2 , 5 , 1];
console.log(nombres);

nombres = nombres.reduce((accumulateur, valeurCourant) => accumulateur + valeurCourant);
console.log('le resumé du tab nombres est : ' + nombres);

//4_Utilisez la méthode .sort() pour trier un tableau de chaînes par ordre alphabétique.

console.log('4_Utilisez la méthode .sort() pour trier un tableau de chaînes par ordre alphabétique.');

chaine.sort();
console.log(chaine);

//5_Utilisez la méthode .forEach() pour imprimer chaque élément d'un tableau.

console.log(`5_Utilisez la méthode .forEach() pour imprimer chaque élément d'un tableau.`);

tab.forEach(nombre => console.log(nombre));
